-module(feed_observer).
-compile(export_all).

-record(state, {feeds,
		clients}).

%%% Interface -----------------------------------------------------------------

start() ->
	register(?MODULE, Pid=spawn(?MODULE, init, [])),
	Pid.

start_link() ->
	register(?MODULE, Pid=spawn_link(?MODULE, init, [])),
	Pid.

stop() ->
	?MODULE ! shutdown.

subscribe(Pid) ->
	Ref = erlang:monitor(process, whereis(?MODULE)),
	?MODULE ! {self(), Ref, {subscribe, Pid}},
	receive
		{Ref, ok} ->
			{ok, Ref};
		{'DOWN', Ref, process, _Pid, Reason} ->
			{error, Reason}
	after 5000 ->
		{error, timeout}
	end.

add_feed(FeedName, CheckInterval) ->
	Ref = make_ref(),
	?MODULE ! {self(), Ref, {add, FeedName, CheckInterval}},
	receive
		{Ref, Msg} ->
			Msg
	after 5000 ->
		{error, timeout}
	end.

cancel_feed(FeedName) ->
	Ref = make_ref(),
	?MODULE ! {self(), Ref, {cancel, FeedName}},
	receive
		{Ref, ok} ->
			ok
	after 5000 ->
		{error, timeout}
	end.

%%% Implementation ------------------------------------------------------------

init() ->
	loop(#state{feeds=orddict:new(),
		    clients=orddict:new()}).

loop(S = #state{}) ->
	receive
		% CLI
		stop ->
			exit(shutdown);
		% Client
		%% subscribe to server notifications
		{Pid, CRef, {subscribe, Client}} ->
			Ref = erlang:monitor(process, Client),
			NewClients = orddict:store(Ref, Client, S#state.clients),
			Pid ! {CRef, ok},
			loop(S#state{clients=NewClients});
		%% client is gone, did not say goodbye
		{'DOWN', Ref, process, _Pid, _Reason} ->
			NewClients = orddict:erase(Ref, S#state.clients),
			loop(S#state{clients=NewClients});
		%% add a feed source
		{Pid, CRef, {add, FeedName, CheckInterval}} ->
			FeedPid = feed:start_link(FeedName, CheckInterval),
			NewFeeds = orddict:store(FeedName, FeedPid, S#state.feeds),
			Pid ! {CRef, ok},
			loop(S#state{feeds=NewFeeds});
		%% cancel a feed source
		{Pid, CRef, {cancel, FeedName}} ->
			NewFeeds = case orddict:find(FeedName, S#state.feeds) of
				{ok, FeedPid} ->
					feed:cancel(FeedPid),
					orddict:erase(FeedName, S#state.feeds);
				error ->
					S#state.feeds
			end,
			Pid ! {CRef, ok},
			loop(S#state{feeds=NewFeeds});
		% Feeds
		%% checked but no new content
		{check, FeedName} ->
			NewFeeds = case orddict:find(FeedName, S#state.feeds) of
				{ok, _FeedPid} ->
					io:fwrite("check"),
					orddict:erase(FeedName, S#state.feeds);
				error ->
					io:fwrite("wut?"),
					S#state.feeds
			end,
			loop(S#state{feeds=NewFeeds});
		% Alien
		code_change ->
			?MODULE:loop(S);
		Unknown ->
			io:format("Unkown message: ~p~n", [Unknown]),
			loop(S)
	end.

