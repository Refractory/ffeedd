%%% Copyright (c) 2013, Max Lapshin
%%% All rights reserved.
%%% 
%%% Redistribution and use in source and binary forms, with or without
%%% modification, are permitted provided that the following conditions are met:
%%%     * Redistributions of source code must retain the above copyright
%%%       notice, this list of conditions and the following disclaimer.
%%%     * Redistributions in binary form must reproduce the above copyright
%%%       notice, this list of conditions and the following disclaimer in the
%%%       documentation and/or other materials provided with the distribution.
%%%     * Neither the name of the <organization> nor the
%%%       names of its contributors may be used to endorse or promote products
%%%       derived from this software without specific prior written permission.
%%% 
%%% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%%% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%%% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%%% DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
%%% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%%% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%%% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
%%% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%%% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%%% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


-module(parsexml).
-export([parse/1]).

-define(IS_WHITESPACE(Char),
    Char == $ orelse
    Char == $\n orelse
    Char == $\r orelse
    Char == $\t
).

-type sxml() :: {atom(), binary() | [sxml()]}.

-spec parse(binary()) -> sxml().
parse(Bin) when is_binary(Bin) ->
    Bin1 = skip_declaration(Bin),
    {Tag, Rest} = tag(Bin1),
    <<>> = trim(Rest),
    Tag.

skip_declaration(<<"<?xml", Bin/binary>>) ->
    [_,Rest] = binary:split(Bin, <<"?>">>),
    trim(Rest),
    skip_declaration(Rest);

skip_declaration(<<"<!", Bin/binary>>) ->
    [_,Rest] = binary:split(Bin, <<">">>),
    trim(Rest);

skip_declaration(<<"<",_/binary>> = Bin) -> Bin;
skip_declaration(<<_,Bin/binary>>) -> skip_declaration(Bin).

trim(<<Blank,Bin/binary>>) when ?IS_WHITESPACE(Blank) ->
    trim(Bin);
trim(Bin) ->
    Bin.

tag(<<"<", Bin/binary>>) ->
    [TagHeader1,Rest1] = binary:split(Bin, <<">">>),
    Len = size(TagHeader1)-1,

    case TagHeader1 of
        <<TagHeader:Len/binary, "/">> ->
            Tag = tag_header(TagHeader),
            {{to_atom(Tag), []}, Rest1};
        TagHeader ->
            Tag = tag_header(TagHeader),
            {Content, Rest2} = tag_content(Rest1, Tag),
            {{to_atom(Tag), Content}, Rest2}
    end.

tag_header(TagHeader) ->
    hd(binary:split(TagHeader, [<<" ">>])).

tag_content(<<Blank,Bin/binary>>, Parent) when ?IS_WHITESPACE(Blank) ->
    tag_content(Bin, Parent);
tag_content(<<"</", Bin1/binary>>, Parent) ->
    Len = size(Parent),
    <<Parent:Len/binary, ">", Bin/binary>> = Bin1,
    {[], Bin};
tag_content(<<"<",_/binary>> = Bin, Parent) ->
    {Tag, Rest1} = tag(Bin),
    {Content, Rest2} = tag_content(Rest1, Parent),
    {[Tag|Content], Rest2};
tag_content(Bin, Parent) ->
    [Text, Rest] = binary:split(Bin, <<"</",Parent/binary,">">>),
    {Text,Rest}.

to_atom(Tag) ->
    binary_to_atom(Tag, latin1).

