-module(feed).
-behavior(gen_server).
-export([init/1, terminate/2, code_change/3]).
-export([handle_call/3, handle_cast/2, handle_info/2]).

-record(feed, {channel,
		name,
		url,
		period,
		last,
		active}).

%%% Admin --------------------------------------------------------------------------------

init([Channel, Name, Url, Period]) ->
	{ok, #feed{channel=Channel, name=Name, url=Url, period=Period, active=true}}.

terminate(Reason, _State) ->
	io:format("Feed terminated, reason: ~p~n", [Reason]).

code_change(_Previous, State, _Extra) ->
	{ok, State}.

%%% Calls --------------------------------------------------------------------------------

handle_call(stop, _From, State) ->
	{stop, normal, ok, State#feed{active=false}}.

%%% Casts --------------------------------------------------------------------------------

handle_cast({check, Name}, State) ->
	io:format("Force checking ~p~n", [Name]),
	check(State),
	{noreply, State};
handle_cast({set_period, Period}, State) ->
	io:format("Seting ~p's period to ~p.~n", [State#feed.name, Period]),
	{noreply, State#feed{period=Period}}.

%%% Infos --------------------------------------------------------------------------------

handle_info(trigger, State=#feed{channel=Channel,name=Name,period=Period}) ->
	erlang:send_after(timer:seconds(Period), self(), trigger),
	io:format("Feed ~p is on check time (~ps)~n", [Name, Period]),
	{Result, NewState} = check(State),
	case Result of
		new ->
			gen_event:notify(Channel, {new, last});
		check ->
			ok
	end,
	{noreply, NewState};
handle_info(Msg, State) ->
	io:format("Unknown message: ~p.~n", [Msg]),
	{noreply, State}.

%%% Extra --------------------------------------------------------------------------------

check(S) ->
UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0",
	io:format("Checking ~p for new content, last was ~p~n",
		[S#feed.url, S#feed.last]),
	{ok, {{_, Code, _}, _, _Body}} =
		httpc:request(get, {S#feed.url, [{"User-Agent", UA}]}, [], []),
	case Code of
		200 ->
			io:format("RSS source returned ~p, analyzing content.~n",
				[Code]);
		_ ->
			io:format("RSS source returned ~p, trying again next time",
				[Code])
	end,		
	{check, S}.

